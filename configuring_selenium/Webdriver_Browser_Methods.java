package configuring_selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Webdriver_Browser_Methods {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	
		System.setProperty("webdriver.chrome.driver", "//Users//carleenamahon//Documents//chromedriver");
		WebDriver driver = new ChromeDriver();
		
		/* Marvel universe will open up in chrome browser first. I used the method navigate().to() 
		 so Chrome tab navigate to DC Universe site. Once load is complete, the method 
		 navigate().back() will bring us back to the the Marvel site. Finally, driver.quit will
		 close all active automation*/
		
		
		driver.get("https://www.marveluniverselive.com/");
		driver.navigate().to("https://www.dcuniverseinfinite.com/");
		driver.navigate().back();
		driver.quit();
	}

}
