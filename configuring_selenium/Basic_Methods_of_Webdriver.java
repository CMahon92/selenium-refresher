package configuring_selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Basic_Methods_of_Webdriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		System.setProperty("webdriver.chrome.driver", "//Users//carleenamahon//Documents//chromedriver");
		WebDriver driver = new ChromeDriver();
		
		//-Used ChromeDriver
		//-Printed title of webpage in console
		//-Confirmed automation landed on the correct URL by printing it in the console
		
		driver.get("http://mauijim.com");
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		
	}

}
