package configuring_selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.safari.SafariDriver;

public class Pagesource {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.Safari", "//usr//bin//safaridriver");
		WebDriver driver = new SafariDriver();
		
		/*Pagesource let you view the HTML or XML code. This is helpful in the event you want
		  to view the pagesource but right click is disabled */
		driver.get("http://discover.com");
		System.out.println(driver.getPageSource());
	
	}

}
