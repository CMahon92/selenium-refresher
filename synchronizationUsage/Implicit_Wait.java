package synchronizationUsage;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Implicit_Wait {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

	
		System.setProperty("webdriver.chrome.driver", "//Users//carleenamahon//Documents//chromedriver");
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		
		String[] itemsNeeded = {"Cucumber", "Tomato", "Pumpkin", "Mango"};
		
		driver.get("https://rahulshettyacademy.com/seleniumPractise/#/");
		addItems(driver, itemsNeeded);
		driver.findElement(By.cssSelector("img[alt='Cart']")).click();
		driver.findElement(By.xpath("//div[contains(@class,'cart-preview')]/div[2]/button")).click();
		driver.findElement(By.cssSelector("input.promoCode")).sendKeys("rahulshettyacademy");
		
		
		
		
	
		
	}
	
	
	public static void addItems (WebDriver driver,String [] itemsNeeded) 
	{
		int j =0;
		
		List<WebElement> produce = driver.findElements(By.cssSelector("h4.product-name"));
		
		for(int i=0;i<produce.size();i++)
			
		{
			
			String[] produceName = produce.get(i).getText().split("-");
			String formattedName = produceName[0].trim();
			
			List itemsNeededList = Arrays.asList(itemsNeeded);
			
					
					if(itemsNeededList.contains(formattedName)) 
					{
						
					j++;
						driver.findElements(By.xpath("//div[@class='product-action']/button")).get(i).click();
						
						if(j==itemsNeeded.length) 
						{
							break;
						}
	}
	

}

	}
}
