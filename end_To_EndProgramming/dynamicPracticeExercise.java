package end_To_EndProgramming;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class dynamicPracticeExercise {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		

			System.setProperty("webdriver.chrome.driver", "//Users//carleenamahon//Documents//chromedriver");
			WebDriverManager.chromedriver().setup();
			WebDriver driver = new ChromeDriver();
			
			driver.get("https://www.rahulshettyacademy.com/AutomationPractice/");
			
			driver.findElement(By.id("checkBoxOption2")).click();
			System.out.println(driver.findElement(By.id("checkBoxOption2")).isEnabled());
			List<WebElement> checkbox = driver.findElements(By.xpath("//div [@class='right-align']/fieldset/label"));
			System.out.println(checkbox.size());
			
			
			for (WebElement checkboxElement : checkbox) 
			{
				String checkboxName =checkboxElement.getText();
				System.out.println("These are the list of checkboxes : " + checkboxName);
				
				if(checkboxElement.isEnabled()) {
					System.out.println("The selected webElement : " + checkboxName);
				}
			}


	}

}
