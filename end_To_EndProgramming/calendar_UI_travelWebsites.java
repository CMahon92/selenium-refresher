package end_To_EndProgramming;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import io.github.bonigarcia.wdm.WebDriverManager;

public class calendar_UI_travelWebsites {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

	
		System.setProperty("webdriver.chrome.driver", "//Users//carleenamahon//Documents//chromedriver");
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.southwest.com/");
		
		//Maximizes Window
		driver.manage().window().maximize();
		driver.findElement(By.id("LandingAirBookingSearchForm_departureDate")).click();
		
		
		while(!driver.findElement(By.xpath("//div[@class='calendar-controls']/div[2]")).getText().contains("June")) 
		{
			driver.findElement(By.xpath("//div[@class='calendar-controls']/button[2]/span[1]")).click();
		} 
		
		
		
		List <WebElement> dates = driver.findElements(By.xpath("//span[@class='actionable--text']"));
		int dateCount = driver.findElements(By.xpath("//*[@id='calendar-66']/div/div[2]/div[10]/button")).size();
		
		for(int i=0;i<dateCount;i++) 
		{
			String text = driver.findElements(By.xpath("//*[@id='calendar-66']/div/div[2]/div[10]/button")).get(i).getText();
			if(text.equalsIgnoreCase("18")) 
			{
				driver.findElements(By.xpath("//*[@id='calendar-66']/div/div[2]/div[10]/button")).get(i).click();
				break;
			}
		} 
		
	}

}
