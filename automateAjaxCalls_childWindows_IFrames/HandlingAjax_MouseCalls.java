package automateAjaxCalls_childWindows_IFrames;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class HandlingAjax_MouseCalls {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "//Users//carleenamahon//Documents//chromedriver");
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.amazon.com/");
		Actions a = new Actions(driver);
		WebElement enterText = driver.findElement(By.id("twotabsearchtextbox"));
		WebElement move = driver.findElement(By.xpath("//a[@id='nav-link-accountList']"));
		
		//Capitalizes characters in text box  :   keyDown(Keys.SHIFT)
		//doubleClick() double clicks the text
		a.moveToElement(enterText).click().keyDown(Keys.SHIFT).sendKeys("christmas").doubleClick().build().perform();
		
		//Moves to specific Element
		//contectClick()  : right click
		a.moveToElement(move).contextClick().build().perform();

	
	}

}
